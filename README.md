# OpenML dataset: Thyroid_Disease

https://www.openml.org/d/46082

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The "Thyroid_Diff.csv" dataset is a comprehensive collection of clinical data relating to thyroid diseases. With attributes capturing a wide range of information from patient demographics (age, gender) to specific clinical findings (smoking history, radiotherapy history, thyroid function, physical examination findings), it provides a detailed overview of patients diagnosed with various forms of thyroid conditions. The dataset encapsulates aspects such as the presence of adenopathy, pathology findings, focality of the disease, and risk categorization. Further, it delves into the TNM classification system, providing insights into the size and extent of tumors (T), presence of cancer in nearby lymph nodes (N), and metastasis (M), thereby contributing to the staging of the disease. The clinical response to treatment and recurrence status is also recorded, offering valuable data for outcomes analysis.

Attribute Description:
- Age: Numeric, represents the age of the patient.
- Gender: Categorical, 'M' for male, 'F' for female.
- Smoking: Binary, 'Yes' if the patient has a history of smoking, 'No' otherwise.
- Hx Smoking: Binary, indicating a historical record of smoking.
- Hx Radiotherapy: Binary, indicates if the patient has undergone radiotherapy.
- Thyroid Function: Categorical, reports the thyroid's functional state.
- Physical Examination: Text, describes findings from physical examination.
- Adenopathy: Binary, 'Yes' if adenopathy is present, 'No' otherwise.
- Pathology: Categorical, type of thyroid pathology diagnosed.
- Focality: Categorical, 'Multi-Focal' or 'Uni-Focal' disease spread.
- Risk: Categorical, assessed risk level ('Low', 'Intermediate', 'High').
- T, N, M: Staging parameters as per the TNM classification.
- Stage: Categorical, stage of the disease.
- Response: Categorical, patient's response to treatment.
- Recurred: Binary, 'Yes' if the disease has recurred, 'No' otherwise.

Use Case:
This dataset is instrumental for researchers and clinicians focusing on thyroid diseases. Its detailed attributes facilitate analyses on the relationship between demographic factors, lifestyle choices (such as smoking), clinical findings, and treatment outcomes. Furthermore, it can serve as a valuable resource for predictive modeling of disease progression, recurrence, and response to therapy. Machine learning applications can leverage this dataset for developing algorithms that predict patient outcomes, guide treatment plans, and assess risk factors for disease recurrence or poor treatment response.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46082) of an [OpenML dataset](https://www.openml.org/d/46082). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46082/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46082/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46082/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

